--SQL DW
SELECT 
	sm.name [schema] ,
	tb.name table_name ,
	tb.create_date,
	tb.modify_date,
	SUM(rg.total_rows) total_rows
FROM sys.schemas sm
	INNER JOIN sys.tables tb ON sm.schema_id = tb.schema_id
	INNER JOIN sys.pdw_table_mappings mp ON tb.object_id = mp.object_id
	INNER JOIN sys.pdw_nodes_tables nt ON nt.name = mp.physical_name
	INNER JOIN sys.dm_pdw_nodes_db_column_store_row_group_physical_stats rg
ON rg.object_id = nt.object_id
	AND rg.pdw_node_id = nt.pdw_node_id
	AND rg.distribution_id = nt.distribution_id
WHERE 1 = 1
GROUP BY sm.name, 
		tb.name,
		tb.create_date,
		tb.modify_date
ORDER BY [schema],table_name


--Redshift
SELECT 
 a.schemaname,
 a.objectname,
SUM(b.reltuples)
FROM 
(
    SELECT 
        schemaname
        ,objectname
    FROM
    (
        SELECT schemaname, 't' AS obj_type, tablename AS objectname, schemaname + '.' + tablename AS fullobj FROM pg_tables
        WHERE schemaname not in ('pg_internal')
        UNION
        SELECT schemaname, 'v' AS obj_type, viewname AS objectname, schemaname + '.' + viewname AS fullobj FROM pg_views
        WHERE schemaname not in ('pg_internal')
        ) AS objs
       
    ORDER BY fullobj
) as a
JOIN 
(
    SELECT 
        nspname AS schemaname,relname,reltuples
    FROM pg_class C
    LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
    ORDER BY reltuples DESC
) as b
ON (a.schemaname = b.schemaname AND a.objectname = b.relname)
WHERE a.schemaname not in ('information_schema', 'pg_catalog')
GROUP BY  a.schemaname,
 a.objectname
 ORDER BY 1,2
 
--Datahub-Impala
show tables;
describe tablename