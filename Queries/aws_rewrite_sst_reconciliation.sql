CREATE TABLE ctx_ai_dev.ab_rewrite_sst_reconciliation
AS
SELECT tx_year,
        CAST(substr(ams_transaction_date,5,2) AS INT) AS tx_month,
        count(1) as row_count,
        SUM(CASE WHEN ams_transaction_type='F' THEN 1 ELSE 0 END) as count_fuel_items,
        SUM(CASE WHEN ams_transaction_type='F' THEN 1 ELSE 0 END) as count_nonfuel_items,
        COUNT(DISTINCT basket_id) as count_distinct_baskets,
        COUNT(basket_lineitem_id) as count_basket_lineitems,
        COUNT(DISTINCT basket_lineitem_id) AS count_distinct_basketline_items,
        SUM(CASE WHEN ams_transaction_type<>'F' THEN COALESCE(total_amount,0) ELSE 0 END) AS total_amount_nonfuel,
        SUM(CASE WHEN ams_transaction_type<>'F' THEN COALESCE(ams_quantity,0) ELSE 0 END) AS total_quantity_non_fuel,
        SUM(CASE WHEN ams_transaction_type<>'F' THEN COALESCE(revenue_inc_gst,0) ELSE 0 END) AS total_revenue_inc_gst_non_fuel,
        SUM(CASE WHEN ams_transaction_type<>'F' THEN COALESCE(ams_discount_amount,0) ELSE 0 END) AS total_ams_discount_amount_non_fuel,
         SUM(CASE WHEN ams_transaction_type='F' THEN COALESCE(total_amount,0) ELSE 0 END) AS total_amount_fuel,
        SUM(CASE WHEN ams_transaction_type='F' THEN COALESCE(ams_quantity,0) ELSE 0 END) AS totsal_volume_fuel,
        SUM(CASE WHEN ams_transaction_type='F' THEN COALESCE(revenue_inc_gst,0) ELSE 0 END) AS total_revenue_inc_gst_fuel,
        SUM(CASE WHEN ams_transaction_type='F' THEN COALESCE(ams_discount_amount,0) ELSE 0 END) AS totsl_ams_discount_amount_fuel
FROM ctx_ai_prod.rewrite_sst
WHERE row_rank=1
--AND CAST(ams_transaction_date AS BIGINT) BETWEEN 20180101 AND 20180829
GROUP BY  tx_year,
          CAST( substr(ams_transaction_date,5,2) AS INT) 
ORDER BY tx_year,
         tx_month 