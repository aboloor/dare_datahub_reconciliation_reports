--Daily reconciliation- AWS
SELECT ams_transaction_date,
		'AWS' AS source_system,
        COUNT(DISTINCT(concat(ams_transaction_date,ams_transaction_sequence,business_loc_id))) as transaction_count,
		count(1) as row_count,
        SUM( ams_quantity) as total_quantity,
        SUM(ams_total_amount)as total_amount,
        SUM(item_reduction_amount) as total_item_reduction_amount,
        SUM(ams_amount) as amount, 
        SUM(item_refund_reduction_amount) as total_item_refund_reduction_amount, 
        SUM(refund_item_amount),
         SUM(COALESCE(ams_total_amount,0)-COALESCE(item_reduction_amount,0)-COALESCE(refund_item_amount,0)+COALESCE(item_refund_reduction_amount,0))as total_Revenue_inc_GST
FROM ctx_core_prod.retail_sales_postx_sb_curated
WHERE CAST(ams_transaction_date AS BIGINT)  >= CAST(from_unixtime(unix_timestamp( to_date(date_sub(current_timestamp(),14)),'yyyy-MM-dd'),'yyyyMMdd')AS BIGINT)
GROUP BY ams_transaction_date,
		source_system
ORDER BY  ams_transaction_date DESC
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT ams_transaction_date,
'AWS' AS source_system,
       COUNT(1) as row_count,
       COUNT(DISTINCT CONCAT(business_loc_id,ams_transaction_date,ams_transaction_sequence))as distinct_transactions_count,
       SUM( collected_amount) as total_collected_amount,
       SUM( tax_amount) as total_tax_amount,
       SUM( transaction_net_amount)as total_transaction_net_amount, 
       SUM(card_payment_amount) as total_card_payment_amount,
       SUM( transaction_net_amount) as total_transaction_net_amount
FROM ctx_core_prod.retail_sales_paymentstx_sb_curated
WHERE CAST(ams_transaction_date AS BIGINT)  >= CAST(from_unixtime(unix_timestamp( to_date(date_sub(current_timestamp(),14)),'yyyy-MM-dd'),'yyyyMMdd')AS BIGINT)
GROUP BY ams_transaction_date,
		source_system
ORDER BY  ams_transaction_date DESC
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT 
       ams_transaction_date,
	   'AWS' AS source_system,
       COUNT(1) as row_count,
      -- COUNT(DISTINCT business_loc_id) AS caltex_site_count,
       COUNT(DISTINCT CONCAT(business_loc_id,ams_transaction_date,ams_transaction_sequence))as distinct_transactions_count,
 SUM(item_gross_sold_amount) as total_gross_sold_amount,
 SUM( item_gross_sold_quantity) as total_item_gross_sold_quantity,
 SUM( item_reduction_amount) as total_item_reduction_amount,
 SUM( item_reduction_quantity)as total_item_reduction_quantity, 
 SUM(vat_net_reduction_amount)as total_vat_net_reduction_amount,
 SUM( vat_tax_rate)as total_vat_tax_rate,
 SUM( item_refund_amt) as total_item_refund_amt,
 SUM( item_refund_qty) as total_item_refund_qty,
 SUM( item_refund_reduction_amount) as total_item_refund_reduction_amount
FROM ctx_core_prod.retail_sales_promotionstx_sb_curated 
WHERE CAST(ams_transaction_date AS BIGINT) >= CAST(from_unixtime(unix_timestamp( to_date(date_sub(current_timestamp(),14)),'yyyy-MM-dd'),'yyyyMMdd')AS BIGINT)
   GROUP BY ams_transction_date,
			source_system   
ORDER BY  ams_transaction_date DESC
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 	
	--Azure
SELECT ams_transaction_date,
		'Azure' AS source_system,
        COUNT(DISTINCT(concat(ams_transaction_date,ams_transaction_sequence,business_loc_id))) as transaction_count,
		count(1) as row_count,
        SUM( ams_quantity) as total_quantity,
        SUM(ams_total_amount)as total_amount,
        SUM(item_reduction_amount) as total_item_reduction_amount,
        SUM(ams_amount) as amount, 
        SUM(item_refund_reduction_amount) as total_item_refund_reduction_amount, 
        SUM(refund_item_amount) as total_refund_item_amount,
         SUM(COALESCE(ams_total_amount,0)-COALESCE(item_reduction_amount,0)-COALESCE(refund_item_amount,0)+COALESCE(item_refund_reduction_amount,0))as total_Revenue_inc_GST
FROM standardised.retail_sales_postx_sb_curated
WHERE CAST(ams_transaction_date AS BIGINT)  >= CAST(from_unixtime(unix_timestamp( to_date(date_sub(current_timestamp(),14)),'yyyy-MM-dd'),'yyyyMMdd')AS BIGINT)
GROUP BY ams_transaction_date,
source_system
ORDER BY  ams_transaction_date DESC

---------------------------------------------------------------------------
SELECT ams_transaction_date,
	'Azure' AS source_system,
       COUNT(1) as row_count,
       COUNT(DISTINCT CONCAT(business_loc_id,ams_transaction_date,ams_transaction_sequence))as distinct_transactions_count,
       SUM( collected_amount) as total_collected_amount,
       SUM( tax_amount) as total_tax_amount,
       SUM( transaction_net_amount)as total_transaction_net_amount, 
       SUM(card_payment_amount) as total_card_payment_amount,
FROM standardised.retail_sales_paymentstx_sb_curated
WHERE CAST(ams_transaction_date AS BIGINT)  >= CAST(from_unixtime(unix_timestamp( to_date(date_sub(current_timestamp(),14)),'yyyy-MM-dd'),'yyyyMMdd')AS BIGINT)
GROUP BY ams_transaction_date,
source_system
ORDER BY  ams_transaction_date DESC
--------------------------------------------------------------------------------

SELECT 
       ams_transaction_date,
	   'Azure' AS source_system,
       COUNT(1) as row_count,
      -- COUNT(DISTINCT business_loc_id) AS caltex_site_count,
       COUNT(DISTINCT CONCAT(business_loc_id,ams_transaction_date,ams_transaction_sequence))as distinct_transactions_count,
 SUM(item_gross_sold_amount) as total_gross_sold_amount,
 SUM( item_gross_sold_quantity) as total_item_gross_sold_quantity,
 SUM( item_reduction_amount) as total_item_reduction_amount,
 SUM( item_reduction_quantity)as total_item_reduction_quantity, 
 SUM(vat_net_reduction_amount)as total_vat_net_reduction_amount,
 SUM( vat_tax_rate)as total_vat_tax_rate,
 SUM( item_refund_amt) as total_item_refund_amt,
 SUM( item_refund_qty) as total_item_refund_qty,
 SUM( item_refund_reduction_amount) as total_item_refund_reduction_amount
FROM satndardised.retail_sales_promotionstx_sb_curated 
WHERE CAST(ams_transaction_date AS BIGINT) >= CAST(from_unixtime(unix_timestamp( to_date(date_sub(current_timestamp(),14)),'yyyy-MM-dd'),'yyyyMMdd')AS BIGINT)
   GROUP BY ams_transction_date,
  source_system   
ORDER BY  ams_transaction_date DESC


